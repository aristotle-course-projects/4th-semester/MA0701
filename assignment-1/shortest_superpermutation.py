from math import factorial
from time import time


def kth_step(k: int, n: int, chars: list):
    """
    Main recursive step of the algorithm.
    :param k: step index
    :param n: alphabet's length
    :param chars: the super-permutation at k-th step
    """
    # Recurse until k becomes zero
    if k > 0:

        # Main algorithm's step
        for i in range(n - k):
            kth_step(k - 1, n, chars)           # k_th step n-k times
            chars += reversed(chars[-n:-n + k])

        # Decrement k and recurse
        kth_step(k - 1, n, chars)


def find_string(alphabet: list) -> list:
    """
    Finds the shortest super-permutation for the given alphabet.
    :param alphabet: an ordered list of integers, [ 1, 2, ..., n ]
    :return: the shortest super-permutation string
    """
    # Length of alphabet
    n = len(alphabet)

    # Initialize return argument
    chars = list(alphabet)

    # Compute string, calling kth_step()
    kth_step(n - 1, n, chars)
    return chars


def check_string(chars: list, alphabet: list) -> bool:
    """
    Check if all permutations of the alphabet are contained in the given string.
    :param chars: the shortest super-permutation string
    :param alphabet: an ordered list of integers, [ 1, 2, ..., n ]
    :return: boolean containing if all permutations of the alphabet are contained in given string
    """
    # Initialize
    n = len(alphabet)
    perm_list = set()
    alphabet_sorted = sorted(alphabet)

    # Check all possible sub-strings of length n and count matches with permutations in alphabet.
    # NOTE:
    #   - permutation: just compare sorted the substring and the alphabet. If matched, then some permutation found.
    #   - if count equals n!, then the produced string is a super-permutation and according to the analysis in the
    #     given link is the shortest one.
    for i in range(len(chars) - n + 1):
        perm = chars[i:i + n]
        if sorted(perm) == alphabet_sorted:
            perm_list.add(tuple(perm))  # add() makes no duplicate entries

    return len(perm_list) == factorial(n)


# Input
_n = int(input("===== INPUT =====\n\t- n = "))
_alphabet = list(range(1, _n + 1))

# Compute
tic = time()
_chars = find_string(_alphabet)
toc = time()
_string = ''.join(str(c) for c in _chars)

# Execution time
_time = toc - tic
_time_units = "seconds"
if _time <= 1:
    _time *= 1e3
    _time_units = "ms"

# Restrict string printing
_nchars = len(_chars)
_NCHARS_MAX = 100
if _nchars > _NCHARS_MAX:
    _string = _string[1:_NCHARS_MAX] + "..." + _string[-3:]

# Print Results
print("\n===== OUTPUT =====")
print("\t- string: \"" + _string + "\"")
print("\t- length: {:,} chars".format(_nchars))
print("\t- time  : {0:.4f} ".format(_time) + _time_units)

print("\n===== CHECK =====")
print("\t- check : " + str(check_string(_chars, _alphabet)))

print(
    "\n===== NOTES =====\n\tThese results are from the solution given to the \"The Haruhi Problem\" by Anonymous ("
    "/sci/).\n\tThe logic and proof of this algorithm can be found in the following "
    "link:\n\t\thttps://mathsci.fandom.com/wiki/The_Haruhi_Problem\n\tAll credits should go to him.\n")
